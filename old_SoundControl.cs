﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class SoundControl {

	static bool initialized=false;
	private static Dictionary<string,AudioSource> loaded_sounds;
	private static Dictionary<string,AudioSource> loaded_musics;
	private static AudioListener camera_listener;
	private static GameObject sound_obj;
	private static float sound_volume=1.0f;
	private static float music_volume=1.0f;
	

	public static AudioSource PlaySound(AudioListener listener, string sound_location, bool restart_if_playing, bool play_once){
		Init ();
		if (loaded_sounds==null)
			loaded_sounds=new Dictionary<string, AudioSource>();
		if (restart_if_playing || play_once || !loaded_sounds.ContainsKey(sound_location) || loaded_sounds[sound_location]==null || !loaded_sounds[sound_location].isPlaying){
			//create empty gameobject if it does not exist yet
			if (sound_obj==null){
				sound_obj=new GameObject();
				sound_obj.name="SoundControl";
			}
			if (play_once){
				AudioClip clip=Resources.Load (sound_location) as AudioClip;
				if (!clip)
					throw new FileNotFoundException("Asset \""+sound_location+"\" not found on Resources folder");
				AudioSource audio=sound_obj.AddComponent<AudioSource>();
				audio.clip=clip;
				//DynamicClick dyn=audio.gameObject.AddComponent<DynamicClick>();
				/*dyn.SetOnUpdate(delegate {
					if (!audio.isPlaying){
						MonoBehaviour.DestroyObject(audio);
						MonoBehaviour.DestroyObject(dyn);
					}
				});*/
				audio.volume=sound_volume;
				audio.Play ();
				return audio;
			} else {
				if (!loaded_sounds.ContainsKey(sound_location)|| loaded_sounds[sound_location]==null){
					AudioClip clip=Resources.Load (sound_location) as AudioClip;
					if (!clip)
						throw new FileNotFoundException("Asset \""+sound_location+"\" not found on Resources folder");
					loaded_sounds[sound_location]=sound_obj.AddComponent<AudioSource>();
					loaded_sounds[sound_location].clip=clip;
				}
				loaded_sounds[sound_location].volume=sound_volume;
				loaded_sounds [sound_location].Play ();
				return loaded_sounds[sound_location];
			}
		}
		if (loaded_sounds.ContainsKey (sound_location) && loaded_sounds [sound_location] != null) {
			return loaded_sounds [sound_location];
		}
		return null;
	}

	public static AudioSource PlaySound(AudioListener listener, string sound_location, bool restart_if_playing){
		return PlaySound (listener, sound_location, restart_if_playing, false);
	}

	public static AudioSource PlaySound(string sound_location, bool restart_if_playing, bool play_once){
		if (camera_listener==null){
			camera_listener=Camera.main.GetComponent<AudioListener>();
		}
		return PlaySound (camera_listener, sound_location, restart_if_playing, play_once);
	}

	public static AudioSource PlaySound(string sound_location, bool restart_if_playing){
		return PlaySound (sound_location, restart_if_playing, false);
	}

	public static AudioSource PlaySound(string sound_location){
		return PlaySound (sound_location, true);
	}

	private static void Init(){
		if (!initialized) {
			initialized=true;
			if (PlayerRemotePrefs.HasKey("music_volume")){
				sound_volume=PlayerRemotePrefs.GetFloat("sound_volume");
				music_volume=PlayerRemotePrefs.GetFloat("music_volume");
			} else {
				SetMusicVolume(music_volume);
				SetSoundVolume(sound_volume);
			}
		}
	}

	public static AudioSource PlayMusic(AudioListener listener, string music_location, bool restart_if_playing, bool play_once){
		Init ();
		if (loaded_musics==null)
			loaded_musics=new Dictionary<string, AudioSource>();
		if (restart_if_playing || play_once || !loaded_musics.ContainsKey(music_location) || loaded_musics[music_location]==null || !loaded_musics[music_location].isPlaying){
			//create empty gameobject if it does not exist yet
			if (sound_obj==null){
				sound_obj=new GameObject();
				sound_obj.name="SoundControl";
			}
			if (play_once){
				AudioClip clip=Resources.Load (music_location) as AudioClip;
				if (!clip)
					throw new FileNotFoundException("Asset \""+music_location+"\" not found on Resources folder");
				AudioSource audio=sound_obj.AddComponent<AudioSource>();
				audio.clip=clip;
				//DynamicClick dyn=audio.gameObject.AddComponent<DynamicClick>();
				/*dyn.SetOnUpdate(delegate {
					if (!audio.isPlaying){
						MonoBehaviour.DestroyObject(audio);
						MonoBehaviour.DestroyObject(dyn);
					}
				});*/
				audio.volume=music_volume;
				audio.Play ();
				return audio;
			} else {
				if (!loaded_musics.ContainsKey(music_location) || loaded_musics[music_location]==null){
					AudioClip clip=Resources.Load (music_location) as AudioClip;
					if (!clip)
						throw new FileNotFoundException("Asset \""+music_location+"\" not found on Resources folder");
					loaded_musics[music_location]=sound_obj.AddComponent<AudioSource>();
					loaded_musics[music_location].clip=clip;
				}
				loaded_musics[music_location].volume=music_volume;
				loaded_musics [music_location].Play ();
				return loaded_musics[music_location];
			}
		}
		if (loaded_musics.ContainsKey (music_location) && loaded_musics[music_location] != null) {
			return loaded_musics[music_location];
		}
		return null;
	}

	public static AudioSource PlayMusic(AudioListener listener, string music_location, bool restart_if_playing){
		return PlayMusic (listener, music_location, restart_if_playing, false);
	}
	
	public static AudioSource PlayMusic(string music_location, bool restart_if_playing, bool play_once){
		if (camera_listener==null){
			camera_listener=Camera.main.GetComponent<AudioListener>();
		}
		return PlayMusic (camera_listener, music_location, restart_if_playing, play_once);
	}

	public static AudioSource PlayMusic(string music_location, bool restart_if_playing){
		return PlayMusic (music_location, restart_if_playing,false);
	}
	
	public static AudioSource PlayMusic(string music_location){
		return PlayMusic (music_location, true);
	}

	public static AudioSource GetSound(string sound_location){
		if (loaded_sounds!=null && loaded_sounds.ContainsKey(sound_location))
			return loaded_sounds [sound_location];
		return null;
	}

	public static AudioSource GetMusic(string music_location){
		if (loaded_musics!=null && loaded_musics.ContainsKey(music_location))
			return loaded_musics [music_location];
		return null;
	}
	
	public static void StopSound(string sound_location){
		AudioSource sound = GetSound (sound_location);
		if (sound && sound.isPlaying)
			sound.Stop();
	}

	public static void StopMusic(string music_location){
		AudioSource music = GetSound (music_location);
		if (music && music.isPlaying)
			music.Stop();
	}

	public static void FadeOutAll(float time){
		if (time >= 1 && loaded_sounds.Count > 0) {
			StopAndClearAll ();
		} else {
			ChangeSoundVolume(time*sound_volume);
			ChangeMusicVolume(time*music_volume);
		}
	}

	public static void StopAndClearAll(){
		if (loaded_sounds!=null){
			foreach (AudioSource source in loaded_sounds.Values) {
				if (source!=null){
					source.Stop();
					MonoBehaviour.DestroyImmediate(source);
				}
			}
			loaded_sounds.Clear();
		}
		if (loaded_musics!=null){
			foreach (AudioSource source in loaded_musics.Values) {
				if (source!=null){
					source.Stop();
					MonoBehaviour.DestroyImmediate(source);
				}
			}
			loaded_musics.Clear();
		}
		MonoBehaviour.DestroyObject(sound_obj);
		sound_obj = null;
		camera_listener = null;
	}

	public static void SetSoundVolume(float volume,bool local_only){
		Init ();
		ChangeSoundVolume (volume);
		if (local_only)
			PlayerPrefs.SetFloat ("sound_volume", volume);
		else
			PlayerRemotePrefs.SetFloat ("sound_volume", volume);
		sound_volume = volume;
	}

	public static void SetMusicVolume(float volume,bool local_only){
		Init ();
		ChangeMusicVolume (volume);
		if (local_only)
			PlayerPrefs.SetFloat ("music_volume", volume);
		else
			PlayerRemotePrefs.SetFloat ("music_volume", volume);
		music_volume = volume;
	}

	
	public static void SetSoundVolume(float volume){
		SetSoundVolume (volume, false);
	}
	
	public static void SetMusicVolume(float volume){
		SetMusicVolume (volume, false);
	}

	private static void ChangeMusicVolume(float volume){
		if (loaded_musics!=null){
			foreach (AudioSource source in loaded_musics.Values) {
				if (source!=null)
					source.volume=volume;
			}
		}
	}

	private static void ChangeSoundVolume(float volume){
		if (loaded_sounds!=null){
			foreach (AudioSource source in loaded_sounds.Values) {
				if (source!=null)
					source.volume=volume;
			}
		}
	}

	public static float GetMusicVolume(){
		Init ();
		return music_volume;
	}

	public static float GetSoundVolume(){
		Init ();
		return sound_volume;
	}
}
