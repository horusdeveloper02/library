﻿using UnityEngine;
using System.Collections;

public class ButtonOverHelper : MonoBehaviour {

	public Vector3 down_translation;
	public Vector3 up_translation;
	public int miliseconds;
	public GameObject target;
	private GameObjectAnimation down_movement;
	private GameObjectAnimation up_movement;
	private GameObjectAnimation normal_movement;
	private bool is_going_down=false;
	private bool is_going_up=false;
	private bool is_going_normal=false;
	private Vector3 original_pos;
	private System.Action after_over;
	private bool is_down=false;
	private bool is_canceled=false;
	private bool called_callback=true;
	

	void Start(){
		if (target == null) {
			target=gameObject;
		}
	}

	void Update() {
		if (!is_going_down && is_going_up) {
			up_movement.Update();
			if (!up_movement.IsRunning()){
				is_going_up=false;
			}
		} else if (!is_going_down && is_going_normal){
			normal_movement.Update();
			if (!normal_movement.IsRunning()){
				is_going_normal=false;
			}
		} else if (is_going_down){
			down_movement.Update();
			if (!down_movement.IsRunning()){
				is_going_down=false;
			}
		} else if (!is_down && after_over!=null){
			if (!is_canceled && !called_callback){
				called_callback=true;
				after_over();
			} 
		}
	}

	void OnMouseExit() {
		if (is_down)
			is_canceled=true;
	}

	void OnMouseEnter() {
		if (is_down)
			is_canceled=false;
	}

	void OnMouseDown() {
		is_down = true;
		is_canceled = false;
		called_callback = false;
		if (!is_going_down) {
			is_going_down=true;
			original_pos = target.transform.localPosition;
			down_movement=new GameObjectAnimationMoveLocal(target, original_pos, original_pos+down_translation,miliseconds);
			down_movement.Start();
		}
	}
	
	void OnMouseUp() {
		is_down = false;
		if (!is_going_up) {
			is_going_up=true;
			up_movement=new GameObjectAnimationMoveLocal(target, original_pos+down_translation, original_pos+up_translation,miliseconds);
			up_movement.Start();
			is_going_normal=true;
			normal_movement=new GameObjectAnimationMoveLocal(target, original_pos+up_translation, original_pos,miliseconds);
			normal_movement.Start();
		}
	}

	public void SetAfterOver(System.Action callback){
		after_over = callback;
	}

}
