﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class CSVToArray {
	
	public static MultiKeyDictionary <int,string,string> LoadFile(string filename, bool store_by_cols){
		char [] trim_colon=new [] { '"' };
		MultiKeyDictionary <int,string,string> result = new MultiKeyDictionary <int,string,string> ();
		/* split the contents of the file into an array of pieces separated by commas */
		TextAsset myTextFile=(TextAsset)Resources.Load(filename,typeof(TextAsset));
		if (myTextFile==null)
			throw new FileNotFoundException("CSV file at \""+filename+"\" not found. Be sure that it exists inside Assets/Resources folder and that you did not pass the extension on the filename parameter");
		//string stringArray = myTextFile.text.Split(","[0]);
		string[] all_lines=myTextFile.text.Split ('\n');
		if (!store_by_cols){
			for (int l=0;l<all_lines.Length;l++) {
				string line=all_lines[l];
				if (line.Length>0){
					string[] col_vals=line.Split('\t');
					if (col_vals.Length>1){
						for (int i=1;i<col_vals.Length;i++) {
							result.Add(i-1,col_vals[0].Trim(trim_colon),col_vals[i].Trim(trim_colon));
							//Debug.Log((i-1)+" -> "+col_vals[0].Trim(trim_colon)+" -> "+col_vals[i].Trim(trim_colon));
						}
					}
				}
			}
		} else {
			if (all_lines.Length>1){
				string[] header=all_lines[0].Split('\t');
				for (int l=1;l<all_lines.Length;l++) {
					string[] col_vals=all_lines[l].Split ('\t');
					for (int i=0;i<col_vals.Length;i++) {
						result.Add(l,header[i].Trim(trim_colon),col_vals[i].Trim(trim_colon));
						//Debug.Log(l+" -> "+header[i].Trim(trim_colon)+" -> "+col_vals[i].Trim(trim_colon));
					}
				}
			}
		}
		return result;
	}
	
	public static MultiKeyDictionary <int,string,string> LoadFile(string filename){
		return LoadFile(filename,true);
	}

}
