﻿/*
GeneralClick
#click #button #delegate
Description:
Used for making objects clickable, given a custom action.
Usage:
Assign it to an object with a collider or collider2D, 
if there are other objects on the scene you may want to give it a rigidbody/rigidbody2D with no gravity, kinematic
On the script you want to assign the action to this gameObject, get the gameObject(let's call it "clickableObj") and do:

	clickableObj.getComponent<GeneralClick>().SetClicking(delegate{
		//your actions here
	});
or
	Action a = delegate{ 
		//your actions
	};
	clickableObj.getComponent<GeneralClick>().SetClicking(a);

WARNING: 
-if the object is instanced again, it will be necessary to reassign that objects Action
-Sometimes is useful to have the gameobject's action set itself to null just so it can only be clicked once, to do that do:
	clickableObj.getComponent<GeneralClick>().SetClicking(delegate{
		//your actions
		clickableObj.getComponent<GeneralClick>().SetClicking(null);
	}
-if you're iterating through many clickable objects with a LOOP be weary of passing the iterating variables,
set them to a local variable before passing them on the delegate. 
	for ex:
for(int i=0; i<10; i++){
	clickableObj.getComponent<GeneralClick>().SetClicking(delegate{
 			doFunction(i);
	});
}
The correct usage would be:
for(int i=0; i<10; i++){
	int temp = i;
	clickableObj.getComponent<GeneralClick>().SetClicking(delegate{
 			doFunction(temp);
	});
}

If you change a global variable that is passed in a delegated action the action will change according to the variable's value
 */

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GeneralClick : MonoBehaviour {
	private bool clickOnIt;
	private Action onClicking;
	private Action onSelecting;
	public void SetSelecting(Action onSelecting){
		this.onSelecting = onSelecting;
	}
	public void SetClicking(Action onClicking){
		this.onClicking=onClicking;

	}
	private void OnMouseDown(){
		//Debug.Log (this.name);
		clickOnIt=true;
		if(onSelecting!=null) onSelecting();
	}
	private void OnMouseExit(){
		clickOnIt=false;
	}
	private void OnMouseUpAsButton () {
		if(clickOnIt){
		//	Debug.Log ("clicking"+this.name);
			if(onClicking!=null) onClicking();
		}
	}

}
