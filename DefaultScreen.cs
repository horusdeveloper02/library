using UnityEngine;
using System.Collections;

public class DefaultScreen {

	public static float DEFAULT_WIDTH=640;
	public static float DEFAULT_HEIGHT=960;
	public static float DEFAULT_ASPECT_RATIO=DefaultScreen.DEFAULT_WIDTH/DefaultScreen.DEFAULT_HEIGHT;
	public static float CURRENT_ASPECT_RATIO=(float)(Screen.width)/Screen.height;

	public enum Position{
		Portrait, LandScape
	}

	public static void definePosition(DefaultScreen.Position pos){
		if (pos==DefaultScreen.Position.Portrait){
			DEFAULT_WIDTH=640;
			DEFAULT_HEIGHT=960;
			DEFAULT_ASPECT_RATIO=DefaultScreen.DEFAULT_WIDTH/DefaultScreen.DEFAULT_HEIGHT;
		} else if (pos==DefaultScreen.Position.LandScape){
			DEFAULT_HEIGHT=640;
			DEFAULT_WIDTH=960;
			DEFAULT_ASPECT_RATIO=DefaultScreen.DEFAULT_WIDTH/DefaultScreen.DEFAULT_HEIGHT;
		}
		Camera.main.aspect = DEFAULT_ASPECT_RATIO;
	}

	public static float adjustByAspectRatio(float value){
		return CURRENT_ASPECT_RATIO * value / DEFAULT_ASPECT_RATIO;
	}

	public static float adjustWidth(float width){
		return Screen.width * width / DEFAULT_WIDTH;
	}

	public static float widthRelativeToDefault(float width){
		return DEFAULT_WIDTH * width / Screen.width;
	}

	public static float adjustHeight(float height){
		return Screen.height * height / DEFAULT_HEIGHT;
	}

	public static float heightRelativeToDefault(float height){
		return DEFAULT_HEIGHT * height / Screen.height;
	}

	public static Vector2 calculatePosition(float x, float y){
		// float left=(middle_point.x-DEFAULT_WIDTH/2);
		// float bottom=(middle_point.y-DEFAULT_HEIGHT/2);
		// float dif_x=left-x;
		// float dif_y=bottom-y;
		// float new_x=adjustWidth(dif_x);
		// if (x<middle_point.x)
		// 	new_x=left+new_x;
		// else
		// 	new_x=DEFAULT_WIDTH-new_x;
		// float new_y=adjustHeight(dif_y);
		// if (y<middle_point.y)
		// 	new_y=bottom+new_y;
		// else
		// 	new_y=DEFAULT_HEIGHT-new_y;
		// return new Vector2(new_x,new_y);
		return new Vector2(adjustWidth(x),adjustHeight(y));
	}

	public static Vector3 calculatePosition(float x, float y, float z){
		Vector2 temp=calculatePosition(x, z);
		return new Vector3(temp.x,y,temp.y);
	}

	public static Vector2 positionToDefaultCoords(float x, float y){
		return new Vector2(widthRelativeToDefault(x),heightRelativeToDefault(y));
	}

	public static Vector3 positionToDefaultCoords(float x, float y, float z){
		Vector2 temp=positionToDefaultCoords(x, z);
		return new Vector3(temp.x,y,temp.y);
	}
	

	public static void updatePosition(GameObject obj, float x, float y){
		Vector2 calculated=calculatePosition(x,y);
		obj.transform.position=new Vector3(calculated.x, obj.transform.position.y, calculated.y);
	}

	public static bool isInsideScreenArea(GameObject obj){
		return obj.transform.position.x <= -Screen.width / 2 - obj.transform.localScale.x / 2 || obj.transform.position.x >= Screen.width / 2 + obj.transform.localScale.x / 2 || obj.transform.position.z <= -Screen.height / 2 - obj.transform.localScale.z / 2 || obj.transform.position.z >= Screen.height / 2 + obj.transform.localScale.z / 2;
	}
	
}
