﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class DynamicClick : MonoBehaviour {

	public Action onTouchDown;
	public Action onTouchUp;
	public Action onRelease;
	public Action onTouchMove;
	public Action onUpdate;
	public static int touchCount=0;

	// Update is called once per frame
	void Update () {
		/*Ray ray;
		RaycastHit hit ;
		if (Input.touches.Length>0){
			DynamicClick.touchCount = Input.touches.Length;
			foreach (Touch touch in Input.touches) {
				ray = Camera.main.ScreenPointToRay (touch.position);
				if (collider && collider.Raycast (ray, out hit,  Mathf.Infinity))
				{
					if (touch.phase == TouchPhase.Began && onTouchDown!=null) {
						onTouchDown();
					}else if (touch.phase == TouchPhase.Moved && onTouchMove!=null) {
						onTouchMove();
					}else if (touch.phase == TouchPhase.Ended && onTouchUp!=null) {
						onTouchUp();
					}
				}
			}
		}*/

		if (onUpdate!=null) {
			onUpdate();
		}
	}
	
	public virtual void SetOnTouchDown(Action onTouchDown){
		this.onTouchDown=onTouchDown;
	}

	public virtual void SetOnTouchMove(Action onTouchMove){
		this.onTouchMove=onTouchMove;
	}
	
	public virtual void SetOnTouchUp(Action onTouchUp){
		this.onTouchUp=onTouchUp;
	}

	public virtual void SetOnRelease(Action onRelease){
		this.onRelease=onRelease;
	}

	public virtual void SetOnUpdate(Action onUpdate){
		this.onUpdate=onUpdate;
	}
	
	void OnMouseDown() {
		//if ((Input.touches.Length==0 || guiTexture!=null || guiText!=null) && onTouchDown!=null) {
		if (onTouchDown!=null) {
			DynamicClick.touchCount=1;
			onTouchDown();
		}
	}
	
	void OnMouseUpAsButton() {
		//if ((Input.touches.Length==0 || guiTexture!=null || guiText!=null) && onTouchUp!=null) {
		if (onTouchUp!=null) {
			onTouchUp();
			DynamicClick.touchCount=0;
		}
	}

	void OnMouseUp() {
		//if ((Input.touches.Length==0 || guiTexture!=null || guiText!=null) && onTouchUp!=null) {
		if (onRelease!=null) {
			onRelease();
			DynamicClick.touchCount=0;
		}
	}
	
	void OnMouseDrag() {
		//if ((Input.touches.Length==0 || guiTexture!=null || guiText!=null) && onTouchMove!=null) {
		if (onTouchMove!=null) {
			onTouchMove();
		}
	}
	
}
