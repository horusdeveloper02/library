﻿using UnityEngine;
using System.Collections;

public class Text3DWrap : MonoBehaviour {

	private TextMesh textObject;
	public int maxLineChars; //maximum number of characters per line...experiment with different values to make it work
	private string[] lines;
	private string[] words;
	private string result = ""; 
	private int charCount;
	private string lastText="";
	
	void Start () {
		if (textObject==null)
			textObject = gameObject.GetComponent<TextMesh> ();
		UpdateText();   //call this function to format your string.
	}

	void Update () {
		UpdateText();
	}

	public void SetText(string text){
		if (textObject==null)
			textObject = gameObject.GetComponent<TextMesh> ();
		textObject.text = text;
		UpdateText ();
	}
	
	public void UpdateText () { 
		FormatString(textObject.text); 
	}
	
	private void FormatString ( string text ) { 
		if (text!=lastText){
			result = ""; 
			lines = text.Replace("/n","\n").Split("\n"[0]); //Split the string into seperate lines
			for (int currentLine=0; currentLine<lines.Length;currentLine++){
				int charCount = 0;
				words = lines[currentLine].Split(" "[0]); //Split the string into seperate words 
				for (int index = 0; index < words.Length; index++) { 
					string word = words[index].Trim(); 
					charCount += word.Length + 1; //+1, because we assume, that there will be a space after every word
					if (charCount <= maxLineChars) {
						if (index==0)
							result += word; 
						else
							result += " " + word; 
					}
					else {
						int word_index=0;
						string text_part="";
						while (word_index+maxLineChars<word.Length){
							text_part=word.Substring(word_index,maxLineChars);
							if (result=="")
								result = text_part;
							else
								result += "\n" + text_part;
							word_index+=maxLineChars;
						} 
						text_part=word.Substring(word_index,word.Length-word_index);
						if (text_part.Length>0){
							charCount = text_part.Length;
							if (result=="")
								result = text_part;
							else
								result += "\n" + text_part; 
						} else {
							charCount=0;
						}
					}
					textObject.text = result; 
				} 
				if (currentLine!=lines.Length-1){
					result += "\n";
				}
			} 
			lastText=result;
		} 
	}

	public int CountTextBreakLines(string text){
		int total_lines=0;
		string [] existent_lines = text.Replace("/n","\n").Split("\n"[0]); //Split the string into seperate lines
		total_lines += existent_lines.Length;
		for (int currentLine=0; currentLine<existent_lines.Length;currentLine++){
			int charCount = 0;
			string [] the_words = existent_lines[currentLine].Split(" "[0]); //Split the string into seperate words 
			for (int index = 0; index < the_words.Length; index++) { 
				string word = the_words[index].Trim(); 
				charCount += word.Length + 1; //+1, because we assume, that there will be a space after every word
				if (charCount > maxLineChars) {
					int word_index=0;
					string text_part="";
					while (word_index+maxLineChars<word.Length){
						text_part=word.Substring(word_index,maxLineChars);
						total_lines++;
						word_index+=maxLineChars;
					}
					text_part=word.Substring(word_index,word.Length-word_index);
					if (text_part.Length>0){
						charCount = text_part.Length+1;
						total_lines++;
					} else {
						charCount=0;
					}
				}
			} 
		} 
		return total_lines;
	}
}
