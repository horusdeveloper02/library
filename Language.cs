﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class Language {
	 
	private static Dictionary<string,string> results=new Dictionary<string, string>();
	private static string cur_language;
	private static string file_location;
	private static Dictionary<int,TextMeshLanguage> all_scene_textes;

	public static void Init(string file_location, string language){
		if (all_scene_textes==null)
			all_scene_textes=new Dictionary<int,TextMeshLanguage>();
		else
			all_scene_textes.Clear();
		object[] all_objs=GameObject.FindObjectsOfTypeAll(typeof(TextMeshLanguage));
		int pos = 0;
		for(int i=0;i<all_objs.Length;i++){
			TextMeshLanguage obj=(TextMeshLanguage)all_objs[i];
			obj.Init();
			TextMesh text_m=obj.GetComponent<TextMesh>();
			if (obj!=null){
				all_scene_textes.Add(pos,obj);
				pos++;
			}
		}
		LoadFileData(file_location, language);
	}

	public static void AddTextMeshAutoTranslation(TextMeshLanguage obj){
		int pos = all_scene_textes.Count;
		obj.mesh.text=Translate(obj.english);
		all_scene_textes.Add(pos,obj);
	}

	public static string Translate(string text){
		return Translate(text,null);
	}

	
	public static string Translate(string text, Dictionary<string,string> changes){
		try{
			string translated = results [text].Replace("\\n","\n");
			if (changes!=null){
				foreach (string key_search in changes.Keys){
					translated=translated.Replace("%"+key_search+"%",changes[key_search]);
				}
			}
			if (translated!=null)
				return translated;
		} catch (KeyNotFoundException e){}
		return text;
	}

	public static bool HasKey(string key_text){
		return results.ContainsKey (key_text);
	}
	

	public static void LoadFileData(string file_location, string language){
		if (all_scene_textes==null)
			throw new System.InvalidOperationException("You need to call Init first.");
		if (language != cur_language) {
			results.Clear();
			Language.file_location=file_location;
			TextAsset myTextFile=(TextAsset)Resources.Load(file_location,typeof(TextAsset));
			if (myTextFile==null)
				throw new FileNotFoundException("Can't load file at \""+file_location+"\" for language \""+language+"\". Be sure that it exists inside Assets/Resources folder and that you did not pass the extension on the filename parameter");
			foreach(string line in myTextFile.text.Split('\n')){
				string [] values=line.Split('\t');
				if (values.Length>1){
					try {
						results.Add(values[0],values[1]);
					} catch (System.ArgumentException e){
						throw new System.ArgumentException("Key \""+values[0]+"\" already exists on file \""+file_location+"\"");
					}
				}
			}
			for(int i=0;i<all_scene_textes.Count;i++){
				TextMeshLanguage obj=all_scene_textes[i];;
				if (obj!=null){
					obj.mesh.text=Translate(obj.english);
				} else {
					all_scene_textes.Remove(i);
				}
			}
		} else if (Language.file_location!=file_location) {
			throw new System.InvalidOperationException("You cannot load another file for the same language.");
		}
	}

}
