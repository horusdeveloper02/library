﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class EventTypeAbstract : MonoBehaviour {

	static Dictionary<EventType,EventTypeController> existent_events_controllers;
	static Dictionary<EventType,bool> existent_events;
	static EventType last_event_type;

	public enum EventType {
		Swipe, Drag, Click, Pinch
	}

	protected abstract Dictionary<EventType,System.Action<EventTypeController.EventStatus>> ExistentEventTypes ();

	protected virtual void OnStart(){}

	public static void UpdateEvents(){
		//Run executed Event
		bool already_executed = false;
		foreach (EventType type in existent_events_controllers.Keys) {
			if (existent_events.ContainsKey(type) && existent_events[type]){
				EventTypeController controller=existent_events_controllers[type];
				controller.OnUpdate();
				EventTypeController.EventStatus status=existent_events_controllers[type].GetCurrentStatus();
				if (status!=EventTypeController.EventStatus.NONE && !already_executed){
					controller.Execute(status);
					already_executed=true;
					last_event_type=type;
				}
			}
		}
	}

	protected virtual void AfterEventUpdate (){}

	protected virtual void BeforeEventUpdate (){}

	void Start(){
		existent_events_controllers = new Dictionary<EventType, EventTypeController> ();
		existent_events = new Dictionary<EventType, bool> ();
		Dictionary<EventType,System.Action<EventTypeController.EventStatus>> all_types = ExistentEventTypes ();
		foreach (EventType type in all_types.Keys) {
			existent_events.Add(type,true);
			existent_events_controllers.Add(type, GetEventTypeController(type,all_types[type]));
		}
		OnStart ();
	}

	void Update(){
		BeforeEventUpdate ();
		UpdateEvents ();
		AfterEventUpdate ();
	}

	private EventTypeController GetEventTypeController(EventType type, System.Action<EventTypeController.EventStatus> the_event){
		if (type==EventType.Click)
			return new EventTypeClick(the_event);
		else if (type==EventType.Swipe)
			return new EventTypeSwipe(the_event);
		else if (type==EventType.Drag)
			return new EventTypeDrag(the_event);
		else if (type==EventType.Pinch)
			return new EventTypePinch(the_event);
		return null;
	}
	
	public static void EnableEventType(EventType[] types){
		foreach (EventType t in types) {
			if (existent_events.ContainsKey(t)){
				existent_events[t]=true;
				existent_events_controllers[t].OnEnable();
			}
		}
	}

	public static void EnableEventType(EventType type){
		EnableEventType (new EventType[]{type});
	}

	public static void EnableAll(){
		EventType[] keys = new EventType[existent_events.Count];
		existent_events.Keys.CopyTo (keys, 0);
		foreach (EventType type in keys) {
			existent_events[type]=true;
			existent_events_controllers[type].OnEnable();
			
		}
	}

	public static void EnableOnly(EventType[] types){
		DisableAll ();
		foreach (EventType t in types) {
			if (existent_events.ContainsKey(t)){
				existent_events[t]=true;
				existent_events_controllers[t].OnEnable();
			}
		}
	}

	public static void EnableOnly(EventType type){
		EnableOnly (new EventType[]{type});
	}

	public static void DisableEventType(EventType[] types){
		foreach (EventType t in types) {
			if (existent_events.ContainsKey(t)){
				existent_events[t]=false;
			}
		}
	}
	
	public static void DisableEventType(EventType type){
		DisableEventType (new EventType[]{type});
	}
	
	public static void DisableAll(){
		EventType[] keys = new EventType[existent_events.Count];
		existent_events.Keys.CopyTo (keys, 0);
		foreach (EventType type in keys) {
			existent_events[type]=false;
		}
	}
	
	public static void DisableOnly(EventType[] types){
		EnableAll ();
		foreach (EventType t in types) {
			if (existent_events.ContainsKey(t)){
				existent_events[t]=false;
			}
		}
	}
	
	public static void DisableOnly(EventType type){
		DisableOnly (new EventType[]{type});
	}

	public static EventType GetLastExecutedEvent(){
		return last_event_type;
	}
}
