﻿using UnityEngine;
using System.Collections;

public class EventTypeSwipe :EventTypeController {

	Vector2 currentSwipe;
	public static Vector2 firstPressPos;
	public static Vector2 secondPressPos;
	EventStatus temporary;
	bool isSwiping=false;
	float MinSwipeDistance = 15f;
	
	public EventTypeSwipe(System.Action<EventStatus> the_event):base(the_event){}

	public override void OnEnable(){
		isSwiping = false;
		firstPressPos = Vector2.zero;
		secondPressPos = Vector2.zero;
		temporary = EventStatus.NONE;
	}

	public override void OnUpdate(){

		if (Input.GetMouseButtonDown (0)) {
			isSwiping=false;
			temporary=EventStatus.NONE;
			firstPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
			secondPressPos=firstPressPos;
		} else if (Input.GetMouseButton (0)) {
			if (!isSwiping){
				secondPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
				currentSwipe = secondPressPos - firstPressPos;
				if ((currentSwipe.magnitude > MinSwipeDistance)){
					if(currentSwipe.y > 0 && (Mathf.Abs(currentSwipe.x) < Mathf.Abs(currentSwipe.y) ))
					{
						isSwiping=true;	
						temporary=EventStatus.DOWN;
					} else if (currentSwipe.y < 0 && (Mathf.Abs(currentSwipe.x) < Mathf.Abs(currentSwipe.y))){
						isSwiping=true;
						temporary=EventStatus.UP;
					} else if (currentSwipe.x > 0 && (Mathf.Abs(currentSwipe.y) < Mathf.Abs(currentSwipe.x))){
						isSwiping=true;
						temporary=EventStatus.LEFT;
					} else {
						isSwiping=true;
						temporary=EventStatus.RIGHT;
					}
				}
			}
		}
	}

	public override EventStatus GetCurrentStatus(){
		if (Input.GetMouseButtonUp (0) && temporary != EventStatus.NONE && !EventTypePinch.has_pinched && !EventTypeDrag.has_started){
			return temporary;
		}
		return EventStatus.NONE;
	}
}
