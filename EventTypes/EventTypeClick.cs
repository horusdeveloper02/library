﻿using UnityEngine;
using System.Collections;

public class EventTypeClick :EventTypeController{


	public EventTypeClick(System.Action<EventStatus> the_event):base(the_event){}
	

	public override EventStatus GetCurrentStatus(){
		if (Input.GetMouseButtonUp (0) && !EventTypeDrag.has_started && !EventTypePinch.has_pinched)
			return EventStatus.UNDEFINED;
		return EventStatus.NONE;
	}
}
