﻿using UnityEngine;
using System.Collections;

public class EventTypePinch :EventTypeController {

	public static bool has_pinched=false;
	public static float start_pinch_distance=0;
	public static float last_distance=0;
	EventStatus temporary;
	EventStatus last_status;

	public EventTypePinch(System.Action<EventStatus> the_event):base(the_event){}

	public override void OnUpdate(){
		temporary=EventStatus.NONE;
		if (Input.touchCount == 2) {
			Vector2 touch0, touch1;
			touch0 = Input.GetTouch(0).position;
			touch1 = Input.GetTouch(1).position;
			if (!has_pinched || last_status==EventStatus.PAUSE){
				start_pinch_distance=Vector2.Distance(touch0,touch1);
				last_distance=Vector2.Distance(touch0,touch1);
				if (!has_pinched){
					temporary=EventStatus.START;
				} else{
					temporary=EventStatus.RESUME;
				}
				has_pinched=true;
			} else {
				last_distance=Vector2.Distance(touch0,touch1);
				if (last_distance==start_pinch_distance)
					temporary=last_status;
				else if (last_distance>start_pinch_distance)
					temporary=EventStatus.OPEN;
				else
					temporary=EventStatus.CLOSE;
			}

		} else if (Input.touchCount ==1 && has_pinched){
			temporary=EventStatus.PAUSE;
		} else if (Input.touchCount ==0 && has_pinched){
			has_pinched=false;
			temporary=EventStatus.END;
		}
		last_status=temporary;
	}

	public override EventStatus GetCurrentStatus(){
		return temporary;
	}
}
