﻿using UnityEngine;
using System.Collections;

public class EventTypeDrag :EventTypeController {

	float dragging_delay = 0.35f;
	float min_drag_distance = 15f;
	public static Vector2 firstPressPos;
	public static Vector2 secondPressPos;
	float dragging_time=0;
	EventStatus temporary;
	bool cur_distance_checked=false;
	public static bool has_started=false;

	public EventTypeDrag(System.Action<EventStatus> the_event):base(the_event){}

	public override void OnUpdate(){
		temporary=EventStatus.NONE;
		if (Input.GetMouseButtonDown (0)) {
			cur_distance_checked=false;
			firstPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
			secondPressPos=firstPressPos;
			dragging_time=0;	
		}else if (Input.GetMouseButton(0)) {
			secondPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
			Vector3 cur_distance = secondPressPos - firstPressPos;
			dragging_time += Time.deltaTime;
			if (cur_distance.magnitude>min_drag_distance){
				cur_distance_checked=true;
			}
			if (dragging_time > dragging_delay && cur_distance_checked){
				if (!has_started){
					has_started=true;
					temporary=EventStatus.START;
				} else {
					temporary=EventStatus.MOVE;
				}
			}
		} else if (Input.GetMouseButtonUp (0) && has_started) {
			temporary=EventStatus.END;
			has_started=false;
		}
	}
	
	public override EventStatus GetCurrentStatus(){
		if (!EventTypePinch.has_pinched){
			return temporary;
		}
		return EventStatus.NONE;
	}
}
