﻿using UnityEngine;
using System.Collections;

public abstract class EventTypeController {

	public enum EventStatus{NONE,OPEN,CLOSE,UP,DOWN,LEFT,RIGHT,UNDEFINED,START,MOVE,END,PAUSE,RESUME};
	public System.Action<EventStatus> the_event;

	public EventTypeController(System.Action<EventStatus> the_event){
		this.the_event = the_event;
	}

	public void Execute(EventStatus status){
		the_event (status);
	}

	public virtual void OnUpdate(){}

	public virtual void OnEnable(){}

	public abstract EventStatus GetCurrentStatus();
}
