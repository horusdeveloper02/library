using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/*
 * ATENTION: This only works for a 2D orthographic camera that have it's position set to (0,0)
 */
public class KeepUpdated{

	private Dictionary<GameObject, ComponentData> data;
	private Camera cam;
	
	public enum AspectMode{
		Scale,Propotional,CallbackOnly
	}

	public KeepUpdated(Camera cam){
		data=new Dictionary<GameObject, ComponentData>();
		this.cam=cam;
		cam.orthographicSize= Screen.height/2;
	}

	/*
	 * Be sure to specify the default scale and the default position of the object for the DEFAULT coordinates specified on DefaultScreen.cs
	 * Those arguments can't be removed otherwise, you could have problems such as making this calculations more then once and then, they become out of the screen.
	 * If you want to position an element on it's position relative to the DEFAULT coordinates, use the function getDefaultScale or getDefaultPosition.
	 * If obj is an instantiated prefab, then this method should always be called next to the instantion and before anything else.
	 */
	public void Register(GameObject obj, Vector3 default_scale, Vector3 default_position, AspectMode type, Action<GameObject,float,float> callback){
		float width=default_scale.x;
		float height=default_scale.z;
		float original_x=default_position.x;
		float original_y=default_position.z;
		ComponentData comp_data=new ComponentData(obj, original_x, original_y, width, height, type, callback);
		cam.orthographicSize= Screen.height/2;
		updateCompData(comp_data);
		data.Add(obj,comp_data);
	}

	public Vector3 getDefaultScale(GameObject obj){
		if (data.ContainsKey (obj))
			return DefaultScreen.positionToDefaultCoords (obj.transform.localScale.x, obj.transform.localScale.y, obj.transform.localScale.z);
		else
			return obj.transform.localScale;
	}

	public Vector3 getDefaultPosition(GameObject obj){
		if (data.ContainsKey (obj))
			return DefaultScreen.positionToDefaultCoords (obj.transform.position.x, obj.transform.position.y, obj.transform.position.z);
		else
			return obj.transform.localScale;
	}

	public void Register(GameObject obj, Vector3 default_scale, Vector3 default_position, AspectMode type){
		Register(obj,default_scale,default_position,type,null);
	}

	private bool updateCompData(ComponentData comp_data){
		if (comp_data==null || comp_data.obj==null) {
			return false;
		} else {
			if (comp_data.mode==AspectMode.Scale){
				UpdateScale(comp_data.obj,comp_data.original_x, comp_data.original_y,comp_data.width,comp_data.height, comp_data.callback);
			} else if (comp_data.mode==AspectMode.Propotional){
				UpdateAspectRatio(comp_data.obj,comp_data.original_x, comp_data.original_y,comp_data.width,comp_data.height, comp_data.callback);
			} else if (comp_data.mode==AspectMode.CallbackOnly){
				CallbackOnly(comp_data.obj, comp_data.original_x, comp_data.original_y, comp_data.callback);
			}
			return true;
		}
	}

	// Scales the element without keeping aspect ratio
	private void UpdateScale (GameObject obj, float original_x, float original_y, float width, float height, Action<GameObject,float,float> callback) {
		obj.transform.localScale = new Vector3(DefaultScreen.adjustWidth(width), 1, DefaultScreen.adjustHeight(height));
		DefaultScreen.updatePosition(obj, original_x,original_y);
		if (callback!=null) {
			callback(obj,original_x,original_y);
		}
	}

	// Scales the element keeping it's aspect ratio. This only updates when the height changes
	private void UpdateAspectRatio(GameObject obj, float original_x, float original_y, float width, float height, Action<GameObject,float,float> callback) {
		if (Screen.height>Screen.width) {
			float aspect_ratio=width/height;
			float new_height=DefaultScreen.adjustHeight(height);
			float new_width=new_height*aspect_ratio;
			obj.transform.localScale = new Vector3(new_width, 1, new_height);
			DefaultScreen.updatePosition(obj, original_x,original_y);
			if (callback!=null) {
				callback(obj,original_x,original_y);
			}
		} else if (Screen.width>Screen.height){
			float aspect_ratio=height/width;
			float new_width=DefaultScreen.adjustWidth(width);
			float new_height=new_width*aspect_ratio;
			obj.transform.localScale = new Vector3(new_width, 1, new_height);
			DefaultScreen.updatePosition(obj, original_x,original_y);
			if (callback!=null) {
				callback(obj,original_x,original_y);
			}
		}
	}

	// Used to configure the element position and other things when the Screen Resolution is updated.
	private void CallbackOnly (GameObject obj, float original_x, float original_y, Action<GameObject,float,float> callback) {
		callback(obj,original_x,original_y);
	}

	// public void UpdateAspectRatio(float width, float height) {
	// 	UpdateAspectRatio(width,height,null);
	// }

	private class ComponentData {
		public GameObject obj;
		public float original_x;
		public float original_y;
		public float width;
		public float height;
		public AspectMode mode;
		public Action<GameObject,float,float> callback;

		public ComponentData(GameObject obj, float original_x, float original_y, float width, float height, AspectMode mode, Action<GameObject,float,float> callback){
			this.obj=obj;
			this.original_x=original_x;
			this.original_y=original_y;
			this.width=width;
			this.height=height;
			this.mode=mode;
			this.callback=callback;
		}
	}
}