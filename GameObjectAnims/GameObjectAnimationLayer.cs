﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationLayer: GameObjectAnimation {

	Direction dir;
	int total_columns;
	int total_rows;
	int total_layers;
	int start_index;
	float offset_per_x;
	float offset_per_y;
	bool loop;
	float time;
	float change_time;

	public enum Direction {
		RightDown
	};

	public GameObjectAnimationLayer (GameObject gameObject, int total_columns, int total_rows, int start_pos, int total_layers, Direction dir, bool loop, int miliseconds): base(gameObject)
	{
		this.time = miliseconds/1000;
		this.total_columns = total_columns;
		this.total_rows = total_rows;
		this.start_index = start_pos;
		this.dir = dir;
		this.accumulated_time = time / total_layers;
		this.total_layers = total_layers;
		this.offset_per_x = 1.0f / total_columns;
		this.offset_per_y = 1.0f / total_rows;
		gameObject.renderer.material.SetTextureScale ("_MainTex", new Vector2(offset_per_x,offset_per_y));
	}

	public GameObjectAnimationLayer (GameObject gameObject, int total_columns, int total_rows, int total_layers, Direction dir, bool loop, int miliseconds): 
		this(gameObject,total_columns,total_rows,0,total_layers, dir,loop, miliseconds){}

	public GameObjectAnimationLayer (GameObject gameObject, int total_columns, int total_rows, Direction dir, bool loop, int miliseconds): 
		this(gameObject,total_columns,total_rows,0,total_columns*total_rows, dir,loop, miliseconds){}

	public GameObjectAnimationLayer (GameObject gameObject, int total_columns, int total_rows, Direction dir, int miliseconds): 
		this(gameObject,total_columns,total_rows,dir,true, miliseconds){}

	protected override bool execute(){
		float accumulated=accumulated_time;
		if (accumulated>time)
			accumulated=time;

		// Calculate index
		int index =GetIndexPos (accumulated);

		// Set new offset
		Vector2 offset = new Vector2 (index%total_columns* this.offset_per_x, 1.0f - (index/total_columns* this.offset_per_y));
		gameObject.renderer.material.SetTextureOffset ("_MainTex", offset);

		if (accumulated == time) {
			if (loop)
				Start();
			else
				return false;
		}
		return true;
	}

	private int GetIndexPos(float accumulated){
		int res=(int)Mathf.Round(accumulated / change_time);
		if (dir==Direction.RightDown){
			if (res>=total_layers)
				res=total_layers-1;
		}
		return res;
	}

}
