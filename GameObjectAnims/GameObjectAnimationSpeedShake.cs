﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationSpeedShake : GameObjectAnimation {

	Vector3 start_position;
	float time;
	int speed;
	int dif=2;
	Direction actual=Direction.Left;

	enum Direction {Left,Right,Up,Down};

	public GameObjectAnimationSpeedShake (GameObject gameObject, int speed, int miliseconds): base(gameObject)
	{
		this.start_position = gameObject.transform.position;
		this.speed = speed/6;
		this.time = miliseconds/1000;
	}
	

	protected override bool execute(){
		float current_move = accumulated_time / time;

		if (actual == Direction.Left)
			MoveLeft ();
		else if (actual==Direction.Right)
			MoveRight ();
		else if (actual==Direction.Up)
			MoveUp ();
		else if (actual==Direction.Down)
			MoveDown ();

		if (current_move>=1) {
			return false;
		}
		return true;
	}

	protected void MoveLeft(){
		float move_x = gameObject.transform.position.x - speed*10*Time.deltaTime;
		if (move_x <= start_position.x - dif && speed>0) {
			speed=-speed;
			move_x=start_position.x - dif;
		} else if (move_x >= start_position.x && speed<0){
			speed=-speed;
			move_x=start_position.x;
			actual=Direction.Up;
		}
		gameObject.transform.position = new Vector3 (move_x, start_position.y, start_position.z);
	}

	protected void MoveRight(){
		float move_x = gameObject.transform.position.x + speed*10*Time.deltaTime;
		if (move_x >= start_position.x + dif && speed>0) {
			speed=-speed;
			move_x=start_position.x + dif;
		} else if (move_x <= start_position.x && speed<0){
			speed=-speed;
			move_x=start_position.x;
			actual=Direction.Down;
		}
		gameObject.transform.position = new Vector3 (move_x, start_position.y, start_position.z);
	}

	protected void MoveUp(){
		float move_y = gameObject.transform.position.y + speed*10*Time.deltaTime;
		if (move_y >= start_position.y + dif && speed>0) {
			speed=-speed;
			move_y=start_position.y + dif;
		} else if (move_y <= start_position.y && speed<0){
			speed=-speed;
			move_y=start_position.y;
			actual=Direction.Right;
		}
		gameObject.transform.position = new Vector3 (start_position.x, move_y, start_position.z);
	}

	protected void MoveDown(){
		float move_y = gameObject.transform.position.y - speed*10*Time.deltaTime;
		if (move_y <= start_position.y - dif && speed>0) {
			speed=-speed;
			move_y=start_position.y - dif;
		} else if (move_y >= start_position.y && speed<0){
			speed=-speed;
			move_y=start_position.y;
			actual=Direction.Left;
		}
		gameObject.transform.position = new Vector3 (start_position.x, move_y, start_position.z);
	}
}
