﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationRotation: GameObjectAnimation {
	
	Quaternion start_angles;
	Quaternion end_angles;
	float time;

	public GameObjectAnimationRotation (GameObject gameObject, Quaternion start_angles, Quaternion end_angles, int miliseconds): base(gameObject)
	{
		this.start_angles = start_angles;
		this.end_angles = end_angles;
		this.time = miliseconds/1000;
	}
	

	protected override bool execute(){
		float current_move = accumulated_time / time;
		gameObject.transform.rotation = Quaternion.Lerp (start_angles, end_angles, current_move);
		if (current_move>=1) {
			return false;
		}
		return true;
	}
}
