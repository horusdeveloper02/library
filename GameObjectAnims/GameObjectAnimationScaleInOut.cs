﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationScaleInOut: GameObjectAnimation {

	Vector3 initial_scale;
	Vector3 end_scale;
	float speed;
	float seconds;

	public GameObjectAnimationScaleInOut (GameObject gameObject, Vector3 initial_scale, Vector3 end_scale, float seconds): base(gameObject)
	{
		this.initial_scale = initial_scale;
		this.end_scale = end_scale;
		this.seconds = seconds;
	}
	

	protected override bool execute(){
		float current_move = accumulated_time/seconds;

		if (current_move<0.5f){
			gameObject.transform.localScale = Vector3.Lerp (initial_scale, end_scale, current_move*2);
		} else {
			gameObject.transform.localScale = Vector3.Lerp (end_scale, initial_scale, (current_move-0.5f)*2);
		}
		
		if (current_move>=1) {
			return false;
		}
		return true;
	}
}
