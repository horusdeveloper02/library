﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationMoveLocal : GameObjectAnimation {

	Vector3 start_position;
	Vector3 end_position;
	float time;
	Easing easing_type;

	public enum Easing{Normal, In, Out}

	public GameObjectAnimationMoveLocal (GameObject gameObject, Vector3 start_position, Vector3 end_position, int miliseconds, Easing easing_type): base(gameObject)
	{
		this.easing_type = easing_type;
		this.start_position = start_position;
		this.end_position = end_position;
		this.time = miliseconds/1000.0f;
	}

	public GameObjectAnimationMoveLocal (GameObject gameObject, Vector3 start_position, Vector3 end_position, int miliseconds): this(gameObject,start_position,end_position,miliseconds,Easing.Normal){}
	

	protected override bool execute(){
		float current_move = accumulated_time / time;
		float value = current_move;
		if (easing_type==Easing.Out)
			value = (float)Cubic.EaseOut (current_move, 0, 1, 1);
		else if (easing_type==Easing.In)
			value = (float)Cubic.EaseIn (current_move, 0, 1, 1);
		gameObject.transform.localPosition = Vector3.Lerp (start_position, end_position, value);
		if (current_move>=1) {
			return false;
		}
		return true;
	}
}
