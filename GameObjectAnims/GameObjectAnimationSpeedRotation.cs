﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationSpeedRotation: GameObjectAnimation {
	
	Quaternion rotation_axis;
	float time;

	public GameObjectAnimationSpeedRotation (GameObject gameObject, Quaternion rotation_axis, int miliseconds): base(gameObject)
	{
		this.rotation_axis = rotation_axis;
		this.time = miliseconds/1000;
	}
	

	protected override bool execute(){
		float current_move = accumulated_time / time;
		gameObject.transform.rotation *= Quaternion.Euler (rotation_axis.eulerAngles.x * 10 * Time.deltaTime, rotation_axis.eulerAngles.y * 10 * Time.deltaTime, rotation_axis.eulerAngles.z * 10 * Time.deltaTime);
		if (current_move>=1) {
			return false;
		}
		return true;
	}
}
