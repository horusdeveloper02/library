﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationCutoff: GameObjectAnimation {

	float time;
	float accumulated_move=0;
	float initial_cut;
	float end_cut;
	int material_number=0;

	public GameObjectAnimationCutoff (GameObject gameObject, float initial_cut, float end_cut, int miliseconds): base(gameObject)
	{
		this.time = miliseconds/1000.0f;
		this.initial_cut = initial_cut;
		this.end_cut = end_cut;
		this.material_number = 0;
	}

	public GameObjectAnimationCutoff (GameObject gameObject, float initial_cut, float end_cut, int miliseconds, int material_number): base(gameObject)
	{
		this.time = miliseconds/1000.0f;
		this.initial_cut = initial_cut;
		this.end_cut = end_cut;
		this.material_number = material_number;
	}
	

	protected override bool execute(){
		float accumulated=accumulated_time / time;
		gameObject.renderer.materials[material_number].SetFloat("_Cutoff",Mathf.Lerp(initial_cut,end_cut,accumulated));
		if (accumulated>=1) {
			return false;
		}
		return true;
	}
}
