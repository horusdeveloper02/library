using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class GameObjectAnimationOpen: GameObjectAnimation {
	
	float time;
	Vector3 initial_pos;
	BombClass gameObjectScript;
	int bombs_per_row=8;
	int margin=1;
	int total_rows=0;
	int total_frags;
	bool all_loaded=false;
	float start_y=0;
	float start_x=0;
	Color initial_color;
	Color end_color;
	float frag_speed;

	public GameObjectAnimationOpen (BombClass gameObjectScript, float frag_speed): base(gameObjectScript.gameObject)
	{
		this.time = 1;
		initial_pos = gameObject.transform.position;
		this.gameObjectScript = gameObjectScript;
		end_color = new Color (gameObject.renderer.material.color.r, gameObject.renderer.material.color.g, gameObject.renderer.material.color.b, 0.0f);
		initial_color = new Color (gameObject.renderer.material.color.r, gameObject.renderer.material.color.g, gameObject.renderer.material.color.b, 1.0f);
		this.frag_speed = frag_speed;
	}

	protected override void onStart(){
		total_frags = gameObjectScript.total_frags;
		total_rows = total_frags / bombs_per_row;
		if (total_frags % bombs_per_row != 0)
			total_rows++;
		//gameObjectScript.StartCoroutine (createFrags ());
		gameObjectScript.StartCoroutine (createFrags ());
		InGameCamera.SwitchGroupVisibility (gameObjectScript.gameObject, false);

	}

	private IEnumerator createFrags(){
		int inc = 0;
		for (int i=0; i<total_frags; i++) {
			//Create the frag
			GameObject the_frag=GameObject.Instantiate(Resources.Load("in game/prefabs/enemies/Frag")) as GameObject;
			/*if (i==0){
				start_y=initial_pos.y+((total_rows)*(the_frag.transform.localScale.y)+(total_rows-1)*(margin))/2+the_frag.transform.localScale.y/2;
			}
			if (i%8==0){
				int total_in_row=bombs_per_row;
				inc+=bombs_per_row;
				if (total_frags<inc)
					total_in_row=bombs_per_row-(inc-total_frags);
				start_x=initial_pos.x-((total_in_row)*(the_frag.transform.localScale.x)+(total_in_row-1)*(margin))/2+the_frag.transform.localScale.x/2;
			} else {
				start_x+=the_frag.transform.localScale.x+margin;
			}
			the_frag.transform.position=new Vector3(start_x,start_y,initial_pos.z);
			if ((i+1)%bombs_per_row==0){
				start_y-=the_frag.transform.localScale.y+margin;
			}*/
			the_frag.transform.position=gameObject.transform.position;
			BombFrag frag_script=the_frag.GetComponent<BombFrag>();
			frag_script.Initialize(FragsAndSplitsPositioning.GetInstance().AssociateFragToPosition(),frag_speed);
			Sequence.GetInstance().frags.Add(frag_script);
			if (i%15==0)
				yield return new WaitForSeconds(0.01f);
		}

		all_loaded = true;
	}

	protected override bool execute(){
		float current_move = accumulated_time / time;
		if (current_move>=1 && all_loaded) {
			return false;
		}
		return true;
	}
}
