﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationSpeedBlink: GameObjectAnimation {

	float time;
	float speed;
	float accumulated_alpha=1;

	public GameObjectAnimationSpeedBlink (GameObject gameObject, int speed, int miliseconds): base(gameObject)
	{
		this.time = miliseconds/1000;
		this.speed = speed/10;
	}
	

	protected override bool execute(){
		float current_move = accumulated_time / time;
		accumulated_alpha -= speed * Time.deltaTime;
		if (accumulated_alpha <= 0) {
			accumulated_alpha=0;
			speed=-speed;
		} else if (accumulated_alpha>=1){
			accumulated_alpha=1;
			speed=-speed;
		}
		gameObject.renderer.material.color = new Color (gameObject.renderer.material.color.r, gameObject.renderer.material.color.g, gameObject.renderer.material.color.b, accumulated_alpha);
		foreach (Transform t in gameObject.transform) {
			t.gameObject.renderer.material.color = new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, accumulated_alpha);
		}
		if (current_move>=1) {
			return false;
		}
		return true;
	}
}
