﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationFadeIn: GameObjectAnimation {

	float time;

	public GameObjectAnimationFadeIn (GameObject gameObject, int miliseconds): base(gameObject)
	{
		this.time = miliseconds/1000.0f;
	}
	

	protected override bool execute(){
		float current_move = accumulated_time / time;
		RecursiveCount (gameObject.transform, current_move);

		if (current_move>=1) {
			return false;
		}
		return true;
	}

	private void RecursiveCount(Transform t,float current_move){
		if (t.childCount>0){
			foreach (Transform t2 in t) {
				RecursiveCount(t2,current_move);
			}
			if (t.gameObject.renderer && t.gameObject.renderer.enabled)
				t.gameObject.renderer.material.color = Color.Lerp (new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, 0.0f), new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, 1.0f), current_move);
		} else if (t.gameObject.renderer.enabled)
			t.gameObject.renderer.material.color = Color.Lerp (new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, 0.0f), new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, 1.0f), current_move);
	}
}
