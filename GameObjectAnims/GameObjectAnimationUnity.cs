﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationUnity: GameObjectAnimation {

	Renderer[] renderers;
	string anim_type;
	bool has_played=false;
	bool disable_renders_start;
	bool disable_renders_end;
	float time;

	public GameObjectAnimationUnity (GameObject gameObject, string anim_type, int miliseconds, bool disable_renders_start, bool disable_renders_end): base(gameObject)
	{
		time = miliseconds/1000.0f;
		this.anim_type = anim_type;
		this.disable_renders_start = disable_renders_start;
		this.disable_renders_end = disable_renders_end;
		//get the renderers and store for later use
		renderers = gameObject.GetComponentsInChildren<Renderer>();
		if (disable_renders_start){
			SwitchRenderers (false);
		}

	}

	public GameObjectAnimationUnity (GameObject gameObject, string anim_type, bool disable_renders_start, bool disable_renders_end): base(gameObject)
	{
		time = gameObject.animation[anim_type].length;
		this.anim_type = anim_type;
		this.disable_renders_start = disable_renders_start;
		this.disable_renders_end = disable_renders_end;
		//get the renderers and store for later use
		renderers = gameObject.GetComponentsInChildren<Renderer>();
		if (disable_renders_start){
			SwitchRenderers (false);
		}
		
	}

	protected override void onStart(){
		if (disable_renders_start)
			SwitchRenderers (true);
		has_played = true;
		accumulated_time = 0;
		gameObject.animation.Play (anim_type);
		gameObject.animation [anim_type].normalizedTime = 0;
	}

	private void SwitchRenderers(bool enabled){
		//turn off renderers initially
		foreach(Renderer r in renderers)
		{
			r.enabled = enabled;
		}
	}
	

	protected override bool execute(){
		float current_move = accumulated_time / time;
		gameObject.animation [anim_type].normalizedTime = current_move;
		if (current_move>=1) {
			if (disable_renders_end)
				SwitchRenderers (false);
			return false;
		}
		return true;
	}
}
