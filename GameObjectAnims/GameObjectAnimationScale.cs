﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationScale : GameObjectAnimation {

	Vector3 initial_scale;
	Vector3 end_scale;
	float seconds;

	public GameObjectAnimationScale (GameObject gameObject, Vector3 initial_scale, Vector3 end_scale, float seconds): base(gameObject)
	{
		this.initial_scale = initial_scale;
		this.end_scale = end_scale;
		this.seconds = seconds;
	}
	

	protected override bool execute(){
		float current_move = accumulated_time / seconds;
		gameObject.transform.localScale = Vector3.Lerp (initial_scale, end_scale, current_move);
		if (current_move>=1) {
			return false;
		}
		return true;
	}
}
