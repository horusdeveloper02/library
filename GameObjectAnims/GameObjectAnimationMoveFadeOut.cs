﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationMoveFadeOut : GameObjectAnimation {

	Vector3 start_position;
	Vector3 end_position;
	float time;
	Easing easing_type;

	public enum Easing{Normal, In, Out}

	public GameObjectAnimationMoveFadeOut (GameObject gameObject, Vector3 start_position, Vector3 end_position, int miliseconds, Easing easing_type): base(gameObject)
	{
		this.easing_type = easing_type;
		this.start_position = start_position;
		this.end_position = end_position;
		this.time = miliseconds/1000.0f;
	}

	public GameObjectAnimationMoveFadeOut (GameObject gameObject, Vector3 start_position, Vector3 end_position, int miliseconds): this(gameObject,start_position,end_position,miliseconds,Easing.Normal){}
	

	protected override bool execute(){
		float current_move = accumulated_time / time;
		float value = current_move;
		if (easing_type==Easing.Out)
			value = (float)Cubic.EaseOut (current_move, 0, 1, 1);
		else if (easing_type==Easing.In)
			value = (float)Cubic.EaseIn (current_move, 0, 1, 1);
		gameObject.transform.position = Vector3.Lerp (start_position, end_position, value);
		RecursiveCount (gameObject.transform, current_move);
		if (current_move>=1) {
			return false;
		}
		return true;
	}

	private void RecursiveCount(Transform t,float current_move){
		if (t.childCount>0){
			foreach (Transform t2 in t) {
				RecursiveCount(t2,current_move);
			}
			if (t.gameObject.renderer && t.gameObject.renderer.enabled)
				t.gameObject.renderer.material.color = Color.Lerp (new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, 1.0f), new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, 0.0f), current_move);
		} else if (t.gameObject.renderer.enabled)
			t.gameObject.renderer.material.color = Color.Lerp (new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, 1.0f), new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, 0.0f), current_move);
	}
}
