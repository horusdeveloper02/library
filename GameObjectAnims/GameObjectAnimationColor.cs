﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationColor: GameObjectAnimation {

	float time;
	float accumulated_move=0;
	Color initial_color;
	Color end_color;
	int material_number=0;

	public GameObjectAnimationColor (GameObject gameObject, Color initial_color, Color end_color, int miliseconds): base(gameObject)
	{
		this.time = miliseconds/1000.0f;
		this.initial_color = initial_color;
		this.end_color = end_color;
		this.material_number = 0;
	}

	public GameObjectAnimationColor (GameObject gameObject, Color initial_color, Color end_color, int miliseconds, int material_number): base(gameObject)
	{
		this.time = miliseconds/1000.0f;
		this.initial_color = initial_color;
		this.end_color = end_color;
		this.material_number = material_number;
	}
	

	protected override bool execute(){
		float accumulated=accumulated_time / time;
		gameObject.renderer.materials[material_number].color = Color.Lerp(initial_color,end_color,accumulated);
		if (accumulated>=1) {
			return false;
		}
		return true;
	}
}
