﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationSpeedColor: GameObjectAnimation {

	float time;
	float accumulated_move=0;
	Color initial_color;
	Color end_color;
	int material_number=0;

	public GameObjectAnimationSpeedColor (GameObject gameObject, Color initial_color, Color end_color, int miliseconds): base(gameObject)
	{
		this.time = miliseconds/1000;
		this.initial_color = initial_color;
		this.end_color = end_color;
		this.material_number = 0;
	}

	public GameObjectAnimationSpeedColor (GameObject gameObject, Color initial_color, Color end_color, int miliseconds, int material_number): base(gameObject)
	{
		this.time = miliseconds/1000;
		this.initial_color = initial_color;
		this.end_color = end_color;
		this.material_number = material_number;
	}
	

	protected override bool execute(){
		float accumulated=accumulated_time / time;
		float current_move = accumulated;
		if (current_move > 0.5f) {
			current_move=1-(current_move);
		}
		accumulated_move = current_move * 2;
		gameObject.renderer.materials[material_number].color = Color.Lerp(initial_color,end_color,accumulated_move);
		if (accumulated>=2) {
			return false;
		}
		return true;
	}
}
