﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationNone: GameObjectAnimation {
	
	Quaternion rotation_axis;
	float time;

	public GameObjectAnimationNone (GameObject gameObject, int miliseconds): base(gameObject)
	{

		this.time = miliseconds/1000;
	}
	

	protected override bool execute(){
		float current_move = accumulated_time / time;
		if (current_move>=1) {
			return false;
		}
		return true;
	}
}
