﻿using UnityEngine;
using System.Collections;

public class GameObjectAnimationFadeInOut: GameObjectAnimation {

	float initial_alpha;
	float end_alpha;
	float seconds;
	float total;
	float out_start;
	float wait_to_out;
	bool total_init_alpha=false;
	System.Action middle_callback;

	public GameObjectAnimationFadeInOut (GameObject gameObject, float initial_alpha, float end_alpha, float seconds, float wait_to_out, System.Action middle_callback): base(gameObject)
	{
		this.initial_alpha = initial_alpha;
		this.end_alpha = end_alpha;
		this.seconds = seconds/2;
		this.wait_to_out = wait_to_out;
		total = seconds + wait_to_out;
		out_start = this.seconds + wait_to_out;
		this.middle_callback = middle_callback;
	}

	public GameObjectAnimationFadeInOut (GameObject gameObject, float initial_alpha, float end_alpha, float seconds, float wait_to_out): this(gameObject,initial_alpha,end_alpha,seconds,wait_to_out,null){}

	public GameObjectAnimationFadeInOut (GameObject gameObject, float initial_alpha, float end_alpha, float seconds): this(gameObject,initial_alpha,end_alpha,seconds,0){}

	protected override void onStart(){
		total_init_alpha=false;
	}

	protected override bool execute(){

		if (accumulated_time<this.seconds){
			Recursive1 (gameObject.transform, accumulated_time/this.seconds);
		} else if (accumulated_time>out_start){
			Recursive2 (gameObject.transform, (accumulated_time-out_start)/this.seconds);
		} else if (!total_init_alpha){
			total_init_alpha=true;
			Recursive1 (gameObject.transform, 1.0f);
			if (middle_callback!=null){
				middle_callback();
			}
		}
		
		if (accumulated_time>=total) {
			return false;
		}
		return true;
	}

	private void Recursive1(Transform t,float current_move){
		if (t.childCount>0){
			foreach (Transform t2 in t) {
				Recursive1(t2,current_move);
			}
			if (t.gameObject.renderer && t.gameObject.renderer.enabled)
				t.gameObject.renderer.material.color = Color.Lerp (new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, initial_alpha), new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, end_alpha), current_move);
		} else if (t.gameObject.renderer.enabled)
			t.gameObject.renderer.material.color = Color.Lerp (new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, initial_alpha), new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, end_alpha), current_move);
	}

	private void Recursive2(Transform t,float current_move){
		if (t.childCount>0){
			foreach (Transform t2 in t) {
				Recursive2(t2,current_move);
			}
			if (t.gameObject.renderer && t.gameObject.renderer.enabled)
				t.gameObject.renderer.material.color = Color.Lerp (new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, end_alpha), new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, initial_alpha), current_move);
		} else if (t.gameObject.renderer.enabled)
			t.gameObject.renderer.material.color = Color.Lerp (new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, end_alpha), new Color (t.gameObject.renderer.material.color.r, t.gameObject.renderer.material.color.g, t.gameObject.renderer.material.color.b, initial_alpha), current_move);
	}
}
