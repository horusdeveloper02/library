﻿using UnityEngine;
using System.Collections;

public abstract class GameObjectAnimation {

	private bool is_running=false;
	private bool has_finished=false;
	public GameObject gameObject;
	public float accumulated_time;

	public GameObjectAnimation(GameObject gameObject){
		this.gameObject = gameObject;
	}

	protected abstract bool execute();

	protected virtual void onStart(){}

	public void Update(){
		if (is_running == true) {
			accumulated_time+=Time.deltaTime;
			if (!execute()){
				is_running=false;
			}
		}
	}

	public void Start(){
		is_running = true;
		accumulated_time = 0;
		onStart ();
	}

	public bool IsRunning(){
		return is_running;
	}

	
	public void ForceStop(){
		is_running = false;
	}
}
