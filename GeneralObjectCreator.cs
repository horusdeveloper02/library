﻿/*
GeneralObjectCreator
Description:
Used for creating or Instantiating new objects, possible to pass local position, scale and rotation,
useful to avoid writing the same lines over and over when wanting to create objects

Usage:
Static functions so just call GeneralObjectCreator.[function] where you want to create objects

Functions:
1-CreateGO
(string name, GameObject parent, Vector3 relative_pos, Vector3 relative_scale, Vector3 relative_rotation)
name - the name of the empty gameobject created
parent - the gameObject of the parent
Optional Arguments:
relative_pos - the relative position to the parent
relative_scale - the relative scale in relation to the parent
relative_rotation - the relative rotation in relation to the parent given in Degrees.

Creates a new gameobject with the given name and parent, int the given relative_position(to the parent), scale and rotation


2-CreateGO_WithLocalPos
(string name, GameObject parent, Vector3 relative_pos)

Same as CreateGO but just changes the position to the relative_pos

3-CreateGO_WithCamPos
(string name, GameObject parent, Vector3 cam_pos)
Same as CreateGO but just changes the position to be relative to the main camera, cam_pos to be given in relation to screen size ex:
if cam_pos is set to: new Vector3(Screen.width/2, Screen.height/2, z_distance_from_main_camera);
then the new GameObject will be at the center of the screen at a certain distance from the camera


4-CreateGO_WithLocalScale
(string name, GameObject parent, Vector3 relative_scale)
Same as CreateGO but just changes the scale to the relative_scale

5-CreateGO_WithLocalRotation
(string name, GameObject parent, Vector3 relative_rotation)
Same as CreateGO but just changes the rotation to the relative_rotation


6-LoadGO
(string path, string name, GameObject parent)
Instantiates a new prefab given a certain path, the name of the gameobject and the parent, path string must lead to a existing prefab inside a Resources folder

7-ResetGO
(GameObject go)
Instantiates a new gameObject go and destroys the old one.

8-ReplaceGO
(GameObject go, string path, string name, Vector3 pos, Vector3 scale, Vector3 rotation)
Replaces a gameObject go, with a gameObject given a path, name, and local position, local scale and local rotation

9-SetGOTransformLocalValues
(GameObject go, Vector3 pos, Vector3 scale, Vector3 rotation)
Changes the local transform values of a given gameObject to pos, scale and rotation

10- SetGOParent
(GameObject go, GameObject parent)
Changes the parent of a given gameObject


11-SetGOOnCamera
(GameObject go, Vector3 cam_pos)
Changes the position of the gameObject to one relative to the main camera


12- RemoveGO 
(GameObject go)
Properly Removes the gameobject and its reference inside a parent, In case the object is inside a parent object 
if we just use destroy the parent keeps a reference to the destroyed object 
wich gives some problems if we want to iterate through the parents children or check how many children it has
 */


using UnityEngine;
using System.Collections;

public class GeneralObjectCreator : MonoBehaviour {
	public static GameObject CreateGO(string name, GameObject parent){
		GameObject go = new GameObject(name);
		go.transform.parent = parent.transform;
		go.transform.localRotation = Quaternion.Euler(Vector3.zero);
		
		return go;
	}

	public static GameObject CreateGO(string name, GameObject parent, Vector3 relative_pos, Vector3 relative_scale, Vector3 relative_rotation){
		GameObject go = CreateGO(name, parent);
		go.transform.localPosition = relative_pos;
		go.transform.localScale = relative_scale;
		go.transform.localRotation = Quaternion.Euler(relative_rotation);
		return go;
	}


	public static GameObject CreateGO_WithLocalPos(string name, GameObject parent, Vector3 relative_pos){
		GameObject go = CreateGO(name, parent);
		go.transform.localPosition = relative_pos;
		return go;
	}


	public static GameObject CreateGO_WithCamPos(string name, GameObject parent, Vector3 cam_pos){
		GameObject go = CreateGO (name, parent);
		go.transform.position = Camera.main.ScreenToWorldPoint(cam_pos);
		return go;
	}
	public static GameObject CreateGO_WithLocalScale(string name, GameObject parent, Vector3 relative_scale){
		GameObject go = CreateGO(name, parent);
		go.transform.localScale = relative_scale;
		return go;
	}

	public static GameObject CreateGO_WithLocalRotation(string name, GameObject parent, Vector3 relative_rotation){
		GameObject go = CreateGO(name, parent);
		go.transform.localRotation = Quaternion.Euler(relative_rotation);
		return go;
	}



	public static GameObject LoadGO(string path, string name, GameObject parent){
		GameObject go = (GameObject) Instantiate(Resources.Load(path));
		go.name = name;
		go.transform.parent = parent.transform;
		go.transform.localRotation = Quaternion.Euler(Vector3.zero);
		return go;
	}
	public static GameObject LoadGO(string path, string name, GameObject parent, Vector3 relative_position, Vector3 relative_scale, Vector3 relative_rotation){
		GameObject go = LoadGO(path, name, parent);
		go.transform.localPosition = relative_position;
		go.transform.localScale = relative_scale;
		go.transform.localRotation = Quaternion.Euler (relative_rotation);
		return go;
	}


	public static GameObject ResetGO(GameObject go){
		GameObject newgo = (GameObject) GameObject.Instantiate(go);
		Vector3 pos = go.transform.position;
		Vector3 scale = go.transform.localScale;
		Quaternion rotation = go.transform.rotation;
		Transform parent = go.transform.parent;
		RemoveGO(go);
		newgo.transform.position = pos;
		newgo.transform.localScale = scale;
		newgo.transform.rotation = rotation;
		newgo.transform.parent = parent;
		return newgo;
	}


	//Can Replace a GameObject with a different gameobject, to replace with an instance of the same gameobject use ResetGO
	public static GameObject ReplaceGO(GameObject go, string path, string name, Vector3 pos, Vector3 scale, Vector3 rotation){
		GameObject newgo = (GameObject) Instantiate(Resources.Load(path));
		newgo.name = name;
		Transform parent = go.transform.parent;
		newgo.transform.parent = parent;
		RemoveGO(go);
		newgo.transform.localPosition = pos;
		newgo.transform.localScale = scale;
		newgo.transform.rotation = Quaternion.Euler(rotation);
		return newgo;
	}

	public static void SetGOTransformLocalValues(GameObject go, Vector3 pos, Vector3 scale, Vector3 rotation){
		go.transform.localPosition = pos;
		go.transform.localScale = scale;
		go.transform.localRotation = Quaternion.Euler(Vector3.zero);
		go.transform.localRotation = Quaternion.Euler(rotation);
	}

	public static void SetGOParent(GameObject go, GameObject parent){
		go.transform.parent = parent.transform;
		go.transform.localRotation = Quaternion.Euler(Vector3.zero);
	}

	public static void SetGOOnCamera(GameObject go, Vector3 cam_pos){
		go.transform.position = Camera.main.ScreenToWorldPoint(cam_pos);
	
	}

	public static void RemoveGO(GameObject go){
		go.transform.parent = null;
		Destroy (go);
	}
}
