﻿using UnityEngine;
using System.Collections;
using System;

public abstract class TextureLoadingAbstract:MonoBehaviour {

	private double progress=0;
	private int current_texture_number=1;
	protected double increase_per_file=0;
	private TextureParams[] all_to_load;
	private bool already_started=false;
	private bool finished_loading=false;

	void Start () {
		BeforeTextureLoad ();
		all_to_load = Load ();
		increase_per_file = 100.00 / all_to_load.Length;
		StartCoroutine ("LoadTextures");
	}

	protected virtual void BeforeTextureLoad(){}

	protected abstract void NewStart ();

	protected abstract void NewUpdate ();

	private IEnumerator LoadTextures() {
		int total = all_to_load.Length;
		foreach (TextureParams to_load in all_to_load) {
			AsyncTexture.Load(to_load.GetResource_texture(),to_load.GetResource_material(),to_load.GetFormat(),to_load.GetWidth(),to_load.GetHeight());
			progress+=increase_per_file;
			if (progress>100.00)
				progress=100.00;
			OnProgress(current_texture_number,total,progress);
			if (progress==100.00){
				finished_loading=true;
			}
			current_texture_number++;
			yield return null;
		}
		if (total == 0) {
			progress=100.00;
			OnProgress(current_texture_number,total,progress);
			if (progress==100.00){
				finished_loading=true;
			}
		}
	}

	protected virtual void OnProgress(int current_texture_number, int total, double progress){
	}

	protected abstract TextureParams[] Load();

	protected class TextureParams{
		private string resource_material;
		private string resource_texture;
		private int width;
		private int height;
		private TextureFormat format;

		public TextureParams(string resource_texture,string resource_material, TextureFormat format, int width, int height){
			this.resource_material=resource_material;
			this.resource_texture=resource_texture;
			this.width=width;
			this.height=height;
			this.format=format;
		}

		public TextureParams(string resource_texture,string resource_material, TextureFormat format): this(resource_texture,resource_material,format,1024,1024){}

		public string GetResource_material (){
			return this.resource_material;
		}

		public string GetResource_texture (){
			return this.resource_texture;
		}

		public int GetWidth (){
			return this.width;
		}

		public int GetHeight (){
			return this.height;
		}

		public TextureFormat GetFormat (){
			return this.format;
		}
	}

	void Update () {
		if (finished_loading && !already_started) {
			already_started = true;
			NewStart ();
		} else if (already_started) {
			NewUpdate();
		}
	}
}
