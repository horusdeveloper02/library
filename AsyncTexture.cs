﻿using UnityEngine;
using System.Collections;

public class AsyncTexture {

	public static Material Load(string texture,string material, TextureFormat format,int width, int height){
		TextAsset text_asset = (TextAsset)Resources.Load (texture);
		Material mat=(Material)Resources.Load (material);
		Texture2D text = new Texture2D (width, height, format, false);
		text.LoadImage (text_asset.bytes);
		mat.mainTexture = text;
		return mat;
	}

	public static Material Load(string texture,string material, TextureFormat format){
		return Load (texture, material, format, 1024, 1024);
	}

}
