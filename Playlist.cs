﻿/*
Playlist
#music #sfx #sound #control
Description:
Used to set and play music or sounds in a given order
Usage:
Set the script to an empty object,
In the editor add the paths of the music or sound files you want the playlist to play
find the object in the script where you want to start the playlist(let's call it playlistObj)
StartPlaylist(); - Initializes the playlist at position 0 
WhatsPlaying - check what file is playing
PlayTrackOnPos(plpos, loopit, pitch) - change the music to a specific file in the given position of the file array
	plpos - is the position on the file array
	loopit - if you want to loop the track after it finishes
	pitch(optional) - if you want to change the pitch of the track
	
 */



using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AudioSource))]
public class Playlist : MonoBehaviour {
	private bool isplaying =false;
	private int plpos = 0;
	public bool loop_playlist=false;
	public string[] playlist = new string[0];
	public int loop_at_pos =-1;

	private int play_mode = 0;
 

	private void Start(){
		this.name = "Playlist";
	//	StartPlaylist();
	}
	// Update is called once per frame
	private void Update () {
		if(play_mode==0){
		if(isplaying){
			//Debug.Log ("is it playing");
			if(!audio.isPlaying){
				if(plpos<playlist.Length){
					if(loop_at_pos!=plpos){
						plpos++;
					}
					if(plpos < playlist.Length){ 
						audio.clip = (AudioClip) Resources.Load (playlist[plpos]);
						audio.Play();
					}
				}else{
					if(loop_playlist){
						StartPlaylist();
					}else{
						plpos=0;
						isplaying=false;
					}
				}
			}//else{
			//	Debug.Log ("playing");
			//}
		}
		}
	}


	public void StartPlaylist(){
		plpos = 0;
		isplaying = true;
		audio.clip = (AudioClip) Resources.Load (playlist[plpos]);
		audio.Play();
	}

	public void PlayTrackOnPos(int plpos, bool loopit ){
		isplaying = true;
		audio.pitch = 1;
		audio.clip = (AudioClip) Resources.Load (playlist[plpos]);
		this.plpos = plpos;
		if(loopit){
			loop_at_pos=plpos;
		}
		audio.Play();
	}



	public void PlayTrackOnPos(int plpos, bool loopit, float pitch){
		isplaying = true;
		audio.pitch = pitch;
		audio.clip = (AudioClip) Resources.Load (playlist[plpos]);
		this.plpos = plpos;
		if(loopit){
			loop_at_pos=plpos;
		}
		audio.Play();
	}


	public string WhatsPlaying(){
		return playlist[this.plpos];
	}

 
}
