﻿/*

General2DControl

Description:
This script, associated with a gameObject, will move that gameObject using either keyboard keys, mouse or touchscreen
Currently there's 3 types of movement: 6 directions, Smooth and Hybrid

Usage:
Associate this script to a gameObject,
Configure the rigidbody2D as: fixedAngle [x], Kinematic [x], gravityScale=0;
Set the other parameters to your needs.

On the script's public variables we have:
forwardMovementSpeed - the move speed constant
Set these to have a certain area of touch or mouse where the gameObject does not move:
DeadZone_X;
DeadZone_Y;


control_type - 
0 : mouse;
1 : keyboard;
2 : touch;

move_type -
0 : 6 Directions - moves only in six directions given a certain interval between them defined by DeadZone_X and DeadZone_Y
1 : Smooth - velocity changes given the direction with no restrictions
2 : Hybrid - it's a mix between 6 Directions and Smooth

can_move_x - if it's possible to move the gameObject on the x axis
can_move_y - if it's possible to move the gameObject on the y axis

Functions:

OnGUI() - For testing purposes, can be commented and never used
MouseControl() - controls the associated gameObject with the mouse
KeyboardControl() - controls the associated gameObject with the keyboard
ScreenControl() - controls the associated gameObject with the touchscreen
MoveHybrid(int x, int y) - moves the gameobject in the given direction using the hybrid type move
MoveSixDirections(int x, int y) - moves the gameobject in the given direction using the 6 directions type move
MoveSmooth(int x, int y) - moves the gameobject in the given direction using the smooth type move
IsInsideMoveArea(Vector3 pos) - checks if a certain point is inside the move area defined by MoveScreenArea_TopLeft and MoveScreenArea_DownRight 
SetMoveArea(int tl_x, int tl_y, int dr_x, int dr_y) sets the values to MoveScreenArea_TopLeft and MoveScreenArea_DownRight, these values are set by default
ResetVelocity() - sets the velocity of the associated gameObject to zero;



*/



using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class General2DControl : MonoBehaviour {

 
	public float forwardMovementSpeed;
	public float DeadZone_X = 0.1f;
	public float DeadZone_Y = 0.1f;

	public int control_type=0;
	public int move_type=0;

	public bool can_move_x=true;
	public bool can_move_y=true;
	private Vector2 MoveScreenArea_TopLeft = new Vector2(0, 0);
	private Vector2 MoveScreenArea_DownRight = new Vector2(Screen.width, Screen.height);

	private void OnGUI(){
			if(GUI.Button(new Rect(0,0, Screen.width/10, Screen.height/10), "mouse")){
				control_type=0;
			}
			if(GUI.Button(new Rect(0, Screen.height/10, Screen.width/10, Screen.height/10), "keyboard")){
				control_type=1;
			}
			if(GUI.Button(new Rect(0, 2*Screen.height/10, Screen.width/10, Screen.height/10), "touch")){
				control_type=2;
			}

			if(GUI.Button(new Rect(Screen.width - Screen.width/10, 0, Screen.width/10, Screen.height/10), "mov 6 dir")){
				move_type=0;
			}
			if(GUI.Button(new Rect(Screen.width - Screen.width/10,  Screen.height/10, Screen.width/10, Screen.height/10), "mov smooth")){
				move_type=1;
			}
			if(GUI.Button(new Rect(Screen.width - Screen.width/10, 2*Screen.height/10, Screen.width/10, Screen.height/10), "mov hybrid")){
				move_type=2;
			}

			if(GUI.Button(new Rect(Screen.width - Screen.width/10, Screen.height - Screen.height/10, Screen.width/10, Screen.height/10), "restart")){
				Application.LoadLevel(Application.loadedLevel);
			}
			DeadZone_X = GUI.HorizontalSlider ( new Rect (0, Screen.height-3f*Screen.height/10, Screen.width/4, Screen.height/10), DeadZone_X, 0.0f, 1f);
			DeadZone_Y = GUI.HorizontalSlider (new Rect (0, Screen.height-2f*Screen.height/10, Screen.width/4, Screen.height/10), DeadZone_Y, 0.0f, 1f);
			forwardMovementSpeed = GUI.HorizontalSlider(new Rect (0, Screen.height-Screen.height/10, Screen.width/4, Screen.height/10), forwardMovementSpeed, 0.0f, 20f);
	}



	private void FixedUpdate (){
		if(control_type==0){
			MouseControl();
		}else if(control_type==1){
			KeyboardControl ();
		}else if(control_type==2){
			ScreenControl ();
		}
	}
	
	private void KeyboardControl(){
		 
			Vector3 cameraLimit = Camera.main.WorldToScreenPoint(transform.position);
			if(Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.W))
			{
				ResetVelocity();
				MoveHybrid(1,1);
				
				
			} else if(Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.S)) {
				ResetVelocity();
				MoveHybrid(1,-1); 
			} else if(Input.GetKey(KeyCode.D)){
				ResetVelocity();
				MoveHybrid(1,0);
			}
			
			
			if(Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W))
			{
				ResetVelocity();
				if(cameraLimit.x>30)
				{
					MoveHybrid(-1,1);
				}else
					MoveHybrid(0,1);
				
			 
				
			} else if(Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.S)) {
				ResetVelocity();
				if(cameraLimit.x>30)
				{
					MoveHybrid(-1,-1);
				}else
					MoveHybrid(0,-1);
				
			 
				
			} else if(Input.GetKey(KeyCode.A)) {
				ResetVelocity();
				 
				if(cameraLimit.x>30)
				{
					MoveHybrid(-1,0);
				}
				
			}
			
			if(Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))
			{
				rigidbody2D.velocity = Vector3.zero;
				rigidbody2D.angularVelocity= 0.0f;
				//				Debug.Log("ONLY W");
				MoveHybrid(0,1);
				
			}
			
			if(Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))
			{
				rigidbody2D.velocity = Vector3.zero;
				rigidbody2D.angularVelocity= 0.0f;
				MoveHybrid(0,-1);
				
			}
			
			if(!Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.W))
			{
				rigidbody2D.velocity = new Vector2 (rigidbody2D.velocity.x*0.8f,rigidbody2D.velocity.y*0.8f);
				rigidbody2D.angularVelocity= 0.0f;
			}

	}
	
	 


	private bool startedInMoveArea = false;
	private void MouseControl(){
		if(Input.GetMouseButtonDown(0)){
			if(IsInsideMoveArea(Input.mousePosition)){
				startjoypos = Input.mousePosition;
				ResetVelocity();
				startedInMoveArea = true;
			}
		}else if(Input.GetMouseButton(0)){
			if(startedInMoveArea==true){
			float tempX=Mathf.Clamp((Input.mousePosition.x-startjoypos.x), -Screen.width/4, Screen.width/4)/(Screen.width/4);
			float tempY=Mathf.Clamp(Input.mousePosition.y-startjoypos.y, -Screen.width/4, Screen.width/4)/(Screen.width/4);
			
			 ResetVelocity();
			if(move_type==0){
				MoveSixDirections(tempX, tempY);
			}else if(move_type==1){
				MoveSmooth(tempX, tempY);
			}else if(move_type==2){
				MoveHybrid(tempX, tempY);	
			}
			}
		}else if(Input.GetMouseButtonUp(0)){
			 ResetVelocity();
			startedInMoveArea = false;
		}
	}


	private Vector3 startjoypos = Vector2.one; //new Vector2(Screen.width, Screen.height);
	private Ray ray;
	private RaycastHit hit;
	private void ScreenControl(){
	 
			if(Input.touchCount>0){
				foreach (Touch t in Input.touches){
					//					if(t.position.x < 3*Screen.width/5){
					if(t.phase==TouchPhase.Began){
						startjoypos = Input.GetTouch(0).position;
						ResetVelocity();
					}else if(t.phase==TouchPhase.Moved || t.phase == TouchPhase.Stationary){
						float tempX=Mathf.Clamp((Input.GetTouch(0).position.x-startjoypos.x), -Screen.width/4, Screen.width/4)/(Screen.width/4);
						float tempY=Mathf.Clamp(Input.GetTouch(0).position.y-startjoypos.y, -Screen.width/4, Screen.width/4)/(Screen.width/4);

					 	ResetVelocity();
				
					if(move_type==0){
						MoveSixDirections(tempX, tempY);
					}else if(move_type==1){
						MoveSmooth(tempX, tempY);
					}else if(move_type==2){
						MoveHybrid(tempX, tempY);	
					}
					 


					}else if(t.phase==TouchPhase.Ended) {
						ResetVelocity();
					}
					 
				}
			}else {
				 ResetVelocity();
			}
 
	}
	
	private void MoveHybrid(float dirX, float dirY){

		Vector2 v = new Vector2( can_move_x ?
		                        	(dirX>-0.8f && dirX<-0.1f) ? -0.8f : 
		                        	(dirX<0.8f && dirX>0.1f) ?  0.8f : dirX  : 0, 
		                        can_move_y ? 
		                        (dirY>-0.8f && dirY<-0.1f) ? -0.8f : 
		                        (dirY<0.8f && dirY>0.1f) ?  0.8f : dirY : 0 );

		//rigidbody2D.AddForce(a*forwardMovementSpeed);
		rigidbody2D.velocity = v*forwardMovementSpeed;
	 }
 


	private void MoveSixDirections(float dirX, float dirY){
		float x = 	can_move_x ?  dirX<-DeadZone_X? -1 : dirX>DeadZone_X? 1 : 0 : 0;
		float y =   can_move_y ?  dirY<-DeadZone_Y ? -1 : dirY>DeadZone_Y? 1 : 0 : 0;
		Vector2 v = new Vector2(x,y);
		rigidbody2D.velocity = v*forwardMovementSpeed;
	}



	private void MoveSmooth(float dirX, float dirY){
		Vector2 v = new Vector2(can_move_x? dirX : 0,  can_move_y ? dirY : 0);
		rigidbody2D.velocity = v*forwardMovementSpeed;
	}



	private void ResetVelocity(){
		rigidbody2D.velocity = Vector3.zero;
		rigidbody2D.angularVelocity= 0.0f;
	}

	private bool IsInsideMoveArea(Vector3 pos){
		return pos.x >= MoveScreenArea_TopLeft.x &&
				pos.x <= MoveScreenArea_DownRight.x &&
				pos.y >= MoveScreenArea_TopLeft.y &&
				pos.y <= MoveScreenArea_DownRight.y;
	}



	public void SetMoveArea(int tl_x, int tl_y, int dr_x, int dr_y){
		MoveScreenArea_TopLeft = new Vector2(tl_x, tl_y);
		MoveScreenArea_DownRight = new Vector2(dr_x, dr_y);
	}
}
