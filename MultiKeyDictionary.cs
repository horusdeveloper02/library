﻿// *************************************************
// Created by Aron Weiler
// Feel free to use this code in any way you like, 
// just don't blame me when your coworkers think you're awesome.
// Comments?  Email aronweiler@gmail.com
// Revision 1.6
// Revised locking strategy based on the some bugs found with the existing lock objects. 
// Possible deadlock and race conditions resolved by swapping out the two lock objects for a ReaderWriterLockSlim. 
// Performance takes a very small hit, but correctness is guaranteed.
// *************************************************

using System.Collections.Generic;
using System.Linq;
using System;

/// <summary>
/// Multi-Key Dictionary Class
/// </summary>	
/// <typeparam name="K">Primary Key Type</typeparam>
/// <typeparam name="L">Sub Key Type</typeparam>
/// <typeparam name="V">Value Type</typeparam>
public class MultiKeyDictionary<K1, K2, V> : Dictionary<K1, Dictionary<K2, V>>  {
	
	public V this[K1 key1, K2 key2] {
		get {
			if (!ContainsKey(key1) || !this[key1].ContainsKey(key2))
				throw new ArgumentOutOfRangeException();
			return base[key1][key2];
		}
		set {
			if (!ContainsKey(key1))
				this[key1] = new Dictionary<K2, V>();
			this[key1][key2] = value;
		}
	}
	
	public void Add(K1 key1, K2 key2, V value) {
		if (!ContainsKey(key1))
			this[key1] = new Dictionary<K2, V>();
		this[key1][key2] = value;
	}
	
	public bool ContainsKey(K1 key1, K2 key2) {
		return base.ContainsKey(key1) && this[key1].ContainsKey(key2);
	}
	
	public new IEnumerable<V> Values {
		get {
			return from baseDict in base.Values
				from baseKey in baseDict.Keys
					select baseDict[baseKey];
		}
	} 
	
}